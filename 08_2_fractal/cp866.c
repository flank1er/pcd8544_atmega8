#include "avr/io.h"
#include "util/delay.h"
#include "pcd8544_soft.h"

int main(void)
{
	int8_t x[17];
	int8_t y[17];
	pcd8544_init();
	pcd8544_clear();
	for (;;)
	{
		pcd8544_clear_fb();

        uint8_t i,j,k;
        for(i=1;i<=4;i++)
        {
            for(j=1;j<=4;j++)
            {
                k=4*i+j-4;
                x[k]=j-3;
                y[k]=i-3;
            }
        }

        x[2]=0;   y[2]=-3;
        x[8]=2;   y[8]=0;
        x[9]=-3;  y[9]=-1;
        x[15]=-1; y[15]=2;

        for(i=1;i<=16;i++)
        {
            for(j=1;j<=16;j++)
            {
                for(k=1;k<=16;k++)
                {
                    int8_t xx=(((x[i]<<4)+(x[j]<<2)+x[k])>>1);
                    int8_t yy=(((y[i]<<4)+(y[j]<<2)+y[k])>>1);
                    pcd8544_set_point(50+xx,28+yy);
                }
            }
        }

		pcd8544_display_fb();

		_delay_ms(1000);
   }

   return 0;
}
