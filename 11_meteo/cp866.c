#include "avr/io.h"
#include "util/delay.h"
#include <avr/pgmspace.h>
#include "pcd8544_soft.h"

#define myabs(n) ((n) < 0 ? -(n) : (n))

const uint8_t blob[7] PROGMEM={112,216,134,129,134,216,112};
const uint8_t degree[7] PROGMEM={0,0,6,9,9,6,0};
const uint8_t degree_big[7] PROGMEM={0,14,27,17,27,14,0};
const uint8_t termometer[7] PROGMEM={0,96,158,129,158,96,0};

int main(void)
{
    // LCD setup
    pcd8544_init();
    /// set variables
    int16_t temp=-117;
    int8_t t1,t2;
    char i=+1;
    // main loop
     for (;;){
        pcd8544_clear_fb();
        // PRINT TIME
        pcd8544_print_at_fb("10:32", FONT_SIZE_2,0,4);
        pcd8544_print_at_fb("07", FONT_SIZE_1,10,5);
        /// print temperature
        t1=temp/10;t2=temp%10;

        if (myabs(t1)>=10) {
            pcd8544_print_uint8_at_fb(myabs(t1),FONT_SIZE_3,4,1);
            if (t1>=0)
                pcd8544_print_at_fb("+", FONT_SIZE_3,1,1);
            else
                pcd8544_print_at_fb("-", FONT_SIZE_3,1,1);
        } else {
            if (t1>=0)
                pcd8544_print_at_fb("+", FONT_SIZE_3,4,1);
            else
                pcd8544_print_at_fb("-", FONT_SIZE_3,4,1);
            pcd8544_print_uint8_at_fb(myabs(t1),FONT_SIZE_3,7,1);
        }
        pcd8544_print_uint8_at_fb(myabs(t2),FONT_SIZE_2,10,2);
        // draw dot
        pcd8544_set_point(68,27);
        pcd8544_set_point(60,27);
        pcd8544_set_point(70,27);
        pcd8544_set_point(68,28);
        pcd8544_set_point(69,28);
        pcd8544_set_point(70,28);
        pcd8544_set_point(68,29);
        pcd8544_set_point(69,29);
        pcd8544_set_point(70,29);
        // draw blob
        char * ptr= (char *)(&blob);
        pcd8544_draw_icon_fb(ptr,0,0,1);
        // draw degree
        ptr= (char *)(&degree);
        pcd8544_draw_icon_fb(ptr,67,0,1);
        ptr= (char *)(&degree_big);
        pcd8544_draw_icon_fb(ptr,67,1,1);
        // draw termometer
        ptr= (char *)(&termometer);
        pcd8544_draw_icon_fb(ptr,48 ,0,1);
        // status bar
        pcd8544_print_at_fb("35%", FONT_SIZE_1,1,0);
        pcd8544_print_at_fb("26", FONT_SIZE_1,8,0);

        pcd8544_display_fb();

        if (temp >= 350)
            i=-1;
        else if (temp <= -350)
            i=1;
        temp+=i;

        _delay_ms(1000);
     }

   return 0;
}
