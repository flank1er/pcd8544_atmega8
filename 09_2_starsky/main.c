#include "avr/io.h"
#include "util/delay.h"
#include "stdlib.h"
#include "pcd8544_soft.h"

#define NUM 120
#define LCD_X 84
#define LCD_Y 48

uint8_t x[NUM];
uint8_t y[NUM];

int main(void)
{
	pcd8544_init();
    pcd8544_clear();
    uint8_t i,j;
    for(i=0;i<NUM;i++)
    {
        x[i]=rand()%(LCD_X);
        y[i]=rand()%(LCD_Y);
    }
    for (;;)
    {
        for(i=0;i<NUM;i++)
        {
            if (x[i] == 0 || y[i] == 0 || x[i] == LCD_X || y[i] == LCD_Y) // border control
            {
                x[i]=rand()%(LCD_X);
                y[i]=rand()%(LCD_Y);
            } else if (x[i] < LCD_X/2 && y[i] < LCD_Y/2) {
                x[i]=x[i]-1;
                y[i]=y[i]-1;
            } else if (x[i] < LCD_X/2 && y[i] >= LCD_Y/2) {
                x[i]=x[i]-1;
                y[i]=y[i]+1;
            } else if (x[i] >=  LCD_X/2 && y[i] <LCD_Y/2) {
                x[i]=x[i]+1;
                y[i]=y[i]-1;
            } else if (x[i] >= LCD_X/2 && y[i] >= LCD_Y/2) {
                x[i]=x[i]+1;
                y[i]=y[i]+1;
            }
        }
        pcd8544_clear_fb();

        for(j=0;j<NUM;j++)
        	pcd8544_set_point(x[j],y[j]);

        pcd8544_display_fb();
        _delay_ms(200);
   }

   return 0;
}
