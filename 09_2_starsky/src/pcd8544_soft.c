#include "ascii.h"
#include "pcd8544_soft.h"

#define FB_LEN  504
static uint8_t fb[FB_LEN];
uint16_t pos;

void pcd8544_init(void)
{
   // GPIO Setup
   DDR_PCD8544 |= (1<<PIN_SCE) | (1<<PIN_RESET) | (1<<PIN_DC) | (1<<PIN_SDIN) | (1<<PIN_SCLK);
   PORT_PCD8544&=~(1<<PIN_RESET);
   PORT_PCD8544|=(1<<PIN_RESET);
   pcd8544_send(LCD_C, 0x21 );  // LCD Extended Commands.
   pcd8544_send(LCD_C, 0xBA );  // Set LCD Vop (Contrast).
   pcd8544_send(LCD_C, 0x04 );  // Set Temp coefficent. //0x04
   pcd8544_send(LCD_C, 0x14 );  // LCD bias mode 1:48. //0x13
   pcd8544_send(LCD_C, 0x20 );  // LCD Basic Commands
   pcd8544_send(LCD_C, 0x0C );  // LCD in normal mode.
}

void pcd8544_send(uint8_t dc, uint8_t data)
{
   uint8_t i;
   if (dc == LCD_D)
      PORT_PCD8544 |= (1<<PIN_DC);
   else
      PORT_PCD8544 &= ~(1<<PIN_DC);

   PORT_PCD8544&=~(1<<PIN_SCE);
   for (i=0; i<8; i++)
   {
      PORT_PCD8544=(data & 0x80) ? PORT_PCD8544 | (1<<PIN_SDIN) : PORT_PCD8544 & ~(1<<PIN_SDIN);

      data=(data<<1);

      PORT_PCD8544|=(1<<PIN_SCLK);
      PORT_PCD8544&=~(1<<PIN_SCLK);
   }
   PORT_PCD8544|=(1<<PIN_SCE);
}

void pcd8544_print_string_fb(char *str)
{
    while (*str && pos < FB_LEN)
    {
        pcd8544_send_char_fb(*str++);
    }
}

void pcd8544_send_char_fb(uint8_t ch)
{
    int i;
    char * ptr= (char *)(&ASCII);

    if (ch >= 0x20 && ch <= 0xf0 && pos <= (FB_LEN-7))
    {
        for (i=0; i < 5; i++)
        {
            uint8_t c=(ch<0xe0) ? ch - 0x20 : ch - 0x50;
            c=pgm_read_byte(ptr+c*5+i);
            fb[pos+i]|=c;
    	}
        pos+=7;
    }
}

void pcd8544_clear(void)
{
   int i;
   for (i=0; i < LCD_X * LCD_Y / 8; i++)
   {
      pcd8544_send(LCD_D, 0x00);
   }

}
void pcd8544_clear_fb(void)
{
    for(pos=0;pos<FB_LEN; pos++)
        fb[pos]=0;
    pos=0;
}

void pcd8544_display_fb() {
    int i;
    for(i=0;i<FB_LEN; i++)
        pcd8544_send(LCD_D,fb[i]);
}

void pcd8544_set_cursor(uint8_t x, uint8_t y) {
    x=x%12; y=y%6;
    pcd8544_send(LCD_C, 0x40+y);
    pcd8544_send(LCD_C, 0x80+x*7);
}

void pcd8544_set_cursor_fb(uint8_t x, uint8_t y) {
    pos=x*7+y*84;
    pos=pos%FB_LEN;
}

void pcd8544_print_at_fb(char *str, uint8_t size,  uint8_t x, uint8_t y)
{
    uint8_t i=0;
    pcd8544_set_cursor_fb(x,y);
    switch (size) {
      case 3:
      while (*str)
      {
         pcd8544_send_char_size3_fb(*str++,x+i,y);
         i+=3;
      }
      break;
      case 2:
      while (*str)
      {
        pcd8544_send_char_size2_fb(*str++,x+i,y);
        i+=2;
      }
      break;
      default:
      while (*str)
      {
        pcd8544_send_char_fb(*str++);
      }
      break;
   }
}

void pcd8544_send_char_size2_fb(uint8_t ch, uint8_t x, uint8_t y) {
    uint8_t s[5]; // source
    uint8_t r[20]; // result
    uint8_t i,j;
   // get littera
    char * ptr= (char *)(&ASCII);
    if (ch >= 0x20 && ch <= 0xf0)
    {
        for (i=0; i < 5; i++)
        {
	          uint8_t c=(ch<0xe0) ? ch - 0x20 : ch - 0x50;
              s[i]=pgm_read_byte(ptr+c*5+i);
        }
    }
    // scale
    for(i=0;i<5;i++)
    {
        uint8_t b=0;
        uint8_t a=0;
        for(j=0;j<4;j++)
        {
            b=(s[i]>>j) & 0x01;
            a|=(b<<(j<<1)) | (b<<((j<<1)+1));
        }
        r[(i<<1)]=a;
        r[(i<<1)+1]=a;
    }

    for(i=0;i<5;i++)
    {
        uint8_t b=0;
        uint8_t a=0;
        for(j=0;j<4;j++)
        {
            b=(s[i]>>(j+4)) & 0x01;
            a|=(b<<(j<<1)) | (b<<((j<<1)+1));
        }
        r[(i<<1)+10]=a;
        r[(i<<1)+11]=a;
    }
    // print
    pcd8544_set_cursor_fb(x,y);
    if (pos<(FB_LEN-14))
    {
        fb[pos++]=0x00; fb[pos++]=0x00;
        for(i=0;i<10;i++)
            fb[pos++]=r[i];

        fb[pos++]=0x00; fb[pos++]=0x00;
    };

    pcd8544_set_cursor_fb(x,y+1);
    if(pos<(FB_LEN-14))
    {
        fb[pos++]=0x00; fb[pos++]=0x00;
        for(i=10;i<20;i++)
            fb[pos++]=r[i];
        fb[pos++]=0x00; fb[pos++]=0x00;
    }
}

void pcd8544_send_char_size3_fb(uint8_t ch, uint8_t x, uint8_t y) {
    uint8_t s[5]; // source
    uint8_t r[45]; // result
    uint8_t i;
    // get littera
    char * ptr= (char *)(&ASCII);
    if (ch >= 0x20 && ch <= 0xf0)
    {
        for (i=0; i < 5; i++)
        {
	          uint8_t c=(ch<0xe0) ? ch - 0x20 : ch - 0x50;
              s[i]=pgm_read_byte(ptr+c*5+i);
        }
    }
    // scale
    for(i=0;i<5;i++)
    {
        uint8_t b,a;
        b=(s[i] & 0x01);
        a=(b) ? 0x7 : 0;
        b=(s[i]>>1) & 0x01;
        if (b) a|=0x38;
        b=(s[i]>>2) & 0x01;
        a|=(b<<6)|(b<<7);

        r[i*3]=a;
        r[i*3+1]=a;
        r[i*3+2]=a;

        r[i*3+15]=b;
        r[i*3+16]=b;
        r[i*3+17]=b;
    }

    for(i=0;i<5;i++)
    {
        uint8_t b,a;
        b=(s[i]>>3) & 0x01;
        a=(b) ? 0x0e : 0;
        b=(s[i]>>4) & 0x01;
        if (b) a|=0x70;
        b=(s[i]>>5) & 0x01;
        a|=(b<<7);

        r[i*3+15]|=a;
        r[i*3+16]|=a;
        r[i*3+17]|=a;
     }

    for(i=0;i<5;i++)
    {
        uint8_t b,a;
        b=(s[i]>>5) & 0x01;
        a=(b) ? 0x3 : 0;
        b=(s[i]>>6) & 0x01;
        if (b) a|=0x1c;
        b=(s[i]>>7) & 0x01;
        if (b) a|=0xe0;

        r[i*3+30]=a;
        r[i*3+31]=a;
        r[i*3+32]=a;
     }

    // print
    pcd8544_set_cursor_fb(x,y);
    fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;
    for(i=0;i<15;i++) fb[pos++]=r[i];
    fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;

    pcd8544_set_cursor_fb(x,y+1);
    fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;
    for(i=15;i<30;i++) fb[pos++]=r[i];
    fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;

    pcd8544_set_cursor_fb(x,y+2);
    fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;
    for(i=30;i<45;i++) fb[pos++]=r[i];
    fb[pos++]=0;fb[pos++]=0; fb[pos++]=0;
}

void pcd8544_print_uint8_at_fb(uint8_t num, uint8_t size, uint8_t x, uint8_t y){
    uint8_t sym[3];
    int8_t i=2;
    do  {
      if (num == 0 && i<2)
        sym[i]=0x20; // space
      else
        sym[i]=0x30+num%10;

      num=num/10;
      i--;

    } while (i>=0);

    uint8_t j=0;
    for (i=0;i<3;i++)
    {
        if (!(i<2 && sym[i] == 0x20))
        {
            switch(size) {
            case 3:
                pcd8544_send_char_size3_fb(sym[i],x+j*size,y);
                break;
            case 2:
                pcd8544_send_char_size2_fb(sym[i],x+j*size,y);
                break;
            default:
                pcd8544_send_char_fb(sym[i]);
                break;
            }
            j++;
        }
    }
}

void pcd8544_set_point(uint8_t x, uint8_t y) {
    if (x < LCD_X && y < LCD_Y)
    {
        uint16_t index = ((y>>3)*LCD_X)+x;
        fb[index]|=(1<<(y&0x07));
    }
}

