#include "avr/io.h"
#include "util/delay.h"
#include "stdlib.h"
#include "pcd8544_soft.h"

#define NUM 40
#define LCD_X 84
#define LCD_Y 48

uint8_t x[NUM];
uint8_t y[NUM];

int main(void)
{
	pcd8544_init();
    pcd8544_clear();
    uint8_t i,j;
    for(i=0;i<NUM;i++)
    {
        x[i]=rand()%(LCD_X/2);
        y[i]=rand()%(LCD_Y/2);
    }
    for (;;){
        for(i=0;i<NUM;i++)
        {
            if (x[i] == 0 || y[i] == 0)
            {
                x[i]=rand()%(LCD_X/2);
                y[i]=rand()%(LCD_Y/2);
            } else {
                x[i]=x[i]-1;
                y[i]=y[i]-1;
            }

        }

        pcd8544_clear_fb();
        for(i=0;i<4;i++)
        {
            for(j=0;j<NUM;j++)
                pcd8544_set_point(x[j],y[j]);
            for(j=0;j<NUM;j++)
                pcd8544_set_point(LCD_X-x[j],y[j]);
            for(j=0;j<NUM;j++)
                pcd8544_set_point(LCD_X-x[j],LCD_Y-y[j]);
            for(j=0;j<NUM;j++)
                pcd8544_set_point(x[j],LCD_Y-y[j]);

        }
	  	pcd8544_display_fb();
     	_delay_ms(300);
   }

   return 0;
}
