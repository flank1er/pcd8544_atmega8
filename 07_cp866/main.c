#include "avr/io.h"
#include "util/delay.h"
#include <avr/pgmspace.h>
#include "pcd8544_soft.h"

#define LEN 72
const unsigned char mystr[LEN] PROGMEM = {"оператор Лапласа это векторный дифференциальный оператор второго порядка"};

int main(void)
{
   pcd8544_init();
   pcd8544_clear();
   char * ptr= (char *)(&mystr);
   uint8_t i; for(i=0;i<LEN;i++)
   {
        pcd8544_send_char(pgm_read_byte(ptr+i));
   }

   for (;;){
      _delay_ms(1000);
   }

   return 0;
}
