#ifndef __PCD8544_SOFT_H__
#define __PCD8544_SOFT_H__

#define PIN_SCE   PD7
#define PIN_RESET PD6
#define PIN_DC    PD5
#define PIN_SDIN  PD4
#define PIN_SCLK  PD3

#define LCD_C     0x00
#define LCD_D     0x01

#define LCD_X     84
#define LCD_Y     48

#define PORT_PCD8544 PORTD
#define DDR_PCD8544  DDRD

extern void pcd8544_init(void);
extern void pcd8544_send(uint8_t dc, uint8_t data);
extern void pcd8544_print_string(char *str);
extern void pcd8544_send_char(uint8_t ch);
extern void pcd8544_clear(void);
extern void pcd8544_set_cursor(uint8_t x, uint8_t y);
extern void pcd8544_print_at(char *str, uint8_t size, uint8_t x, uint8_t y);
extern void pcd8544_send_char_size2(uint8_t ch, uint8_t x, uint8_t y);
extern void pcd8544_send_char_size3(uint8_t ch, uint8_t x, uint8_t y);
extern void pcd8544_print_uint8_at(uint8_t num, uint8_t size, uint8_t x, uint8_t y);

#endif
