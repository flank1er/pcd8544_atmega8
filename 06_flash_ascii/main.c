#include "avr/io.h"
#include "util/delay.h"
#include "pcd8544_soft.h"

int main(void)
{
   pcd8544_init();
   uint8_t i=0;
   for (;;){
      pcd8544_clear();
      pcd8544_print_at(">",3,0,2);
      pcd8544_print_uint8_at(i++,3,3,2);
      _delay_ms(1000);
   }

   return 0;
}
