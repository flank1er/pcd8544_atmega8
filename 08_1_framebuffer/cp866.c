#include "avr/io.h"
#include "util/delay.h"
#include "pcd8544_soft.h"

int main(void)
{
   pcd8544_init();
   pcd8544_clear();
   uint8_t i=0;
   for (;;){
      pcd8544_clear_fb();
      pcd8544_print_at_fb(">",3,0,2);
      pcd8544_print_uint8_at_fb(i++,3,3,2);
	  pcd8544_display_fb();
      _delay_ms(1000);
   }

   return 0;
}
