#include "avr/io.h"
#include "util/delay.h"
#include <avr/pgmspace.h>
#include "pcd8544_hw.h"

// SPI
#define PIN_SCE   PB2
#define PIN_SDIN  PB3
#define PIN_SCLK  PB5
// icons
const uint8_t pointer[7] PROGMEM={0x2,0x6,0xc,0x18,0xc,0x6,0x2};
const uint8_t stereo[7] PROGMEM={0x3c,0x66,0xc3,0x81,0xc3,0x66,0x3c};
static const uint8_t battery[14] PROGMEM={28,34,65,93,93,65,93, 93,65,93,93,65,127,0};

int main(void) {
	// SPI setup
	DDRB |= (1<<PIN_SCE) | (1<<PIN_SDIN) | (1<<PIN_SCLK);
	SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR0); // SPI mode 0, master, prescaler 1/16
    // LCD setup
    pcd8544_init(PIN_SCE);
    /// set variables
    uint16_t freq=1017;
    char i=1;
    uint8_t f1,f2;
    uint8_t j;
    // main loop
    for(;;)
    {
         pcd8544_clear_fb();
        /// print frequency
        f1=freq/10;f2=freq%10;
        if (f1>=100)
            pcd8544_print_uint8_at_fb(f1,FONT_SIZE_3,0,1);
        else
            pcd8544_print_uint8_at_fb(f1,FONT_SIZE_3,3,1);

        pcd8544_print_uint8_at_fb(f2,FONT_SIZE_2,9,2);

        // draw dot
        pcd8544_set_point(61,27);
        pcd8544_set_point(62,27);
        pcd8544_set_point(63,27);
        pcd8544_set_point(61,28);
        pcd8544_set_point(62,28);
        pcd8544_set_point(63,28);
        pcd8544_set_point(61,29);
        pcd8544_set_point(62,29);
        pcd8544_set_point(63,29);
        // status bar
        pcd8544_print_at_fb("M��", FONT_SIZE_1,9,1);
        pcd8544_print_at_fb("��", FONT_SIZE_1,3,0);
        pcd8544_print_at_fb("60dB", FONT_SIZE_1,6,0);
        pcd8544_print_at_fb("7-���� �����", FONT_SIZE_1,0,4);
        //
        pcd8544_draw_line(0,46,83,46);
        for(j=0;j<14;j++)
            pcd8544_draw_line(j*6,44,j*6,47);
        pcd8544_draw_line(83,44,83,47);
        char * ptr= (char *)(&pointer);
        uint8_t p=(freq-880)/2-(freq-880)/10;
        pcd8544_draw_icon_fb(ptr,p,5,1);

        ptr= (char *)(&stereo);
        pcd8544_draw_icon_fb(ptr,0,0,1);
        pcd8544_draw_icon_fb(ptr,7,0,1);

        ptr= (char *)(&battery);
        pcd8544_draw_icon_fb(ptr,70,0,2);

        pcd8544_display_fb();

        if (freq >= 1080)
            i=-1;
        else if (freq <=880)
            i=1;
       freq+=i;

         _delay_ms(1000);
     }

   return 0;
}
