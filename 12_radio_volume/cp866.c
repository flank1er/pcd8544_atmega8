#include "avr/io.h"
#include "util/delay.h"
#include <avr/pgmspace.h>
#include "pcd8544_soft.h"

const uint8_t pointer[7] PROGMEM={0x2,0x6,0xc,0x18,0xc,0x6,0x2};
const uint8_t stereo[7] PROGMEM={0x3c,0x66,0xc3,0x81,0xc3,0x66,0x3c};
static const uint8_t battery[14] PROGMEM={28,34,65,93,93,65,93, 93,65,93,93,65,127,0};

int main(void) {
    // LCD setup
    pcd8544_init();
    /// set variables
    char volume=100;
    uint16_t freq=1017;
    char i=1;
    uint8_t f1,f2;
    // main loop
    for(;;)
    {
         pcd8544_clear_fb();
        /// print frequency
        f1=freq/10;f2=freq%10;
        if (f1>=100)
            pcd8544_print_uint8_at_fb(f1,FONT_SIZE_3,0,1);
        else
            pcd8544_print_uint8_at_fb(f1,FONT_SIZE_3,3,1);

        pcd8544_print_uint8_at_fb(f2,FONT_SIZE_2,9,2);

        // draw dot
        pcd8544_set_point(61,27);
        pcd8544_set_point(62,27);
        pcd8544_set_point(63,27);
        pcd8544_set_point(61,28);
        pcd8544_set_point(62,28);
        pcd8544_set_point(63,28);
        pcd8544_set_point(61,29);
        pcd8544_set_point(62,29);
        pcd8544_set_point(63,29);
        // status bar
        pcd8544_print_at_fb("M��", FONT_SIZE_1,9,1);
        pcd8544_print_at_fb("��", FONT_SIZE_1,3,0);
        pcd8544_print_at_fb("60dB", FONT_SIZE_1,6,0);
        pcd8544_print_at_fb("7-���� �����", FONT_SIZE_1,0,4);
        //
        pcd8544_print_at_fb("��", FONT_SIZE_1,0,5);
        pcd8544_print_uint8_at_fb(volume, FONT_SIZE_1,3,5);
        pcd8544_draw_line(33,45,83,45);
        //
        char * ptr= (char *)(&pointer);
        uint8_t p=33+(volume>>1);
        pcd8544_draw_icon_fb(ptr,p,5,1);

        ptr= (char *)(&stereo);
        pcd8544_draw_icon_fb(ptr,0,0,1);
        pcd8544_draw_icon_fb(ptr,7,0,1);

        ptr= (char *)(&battery);
        pcd8544_draw_icon_fb(ptr,70,0,2);

        pcd8544_display_fb();

        if (volume >= 100)
            i=-1;
        else if (volume <= 0)
            i=1;

        volume+=i;

         _delay_ms(1000);
     }

   return 0;
}
